"""Регистрация на guru99.com."""

import pytest
from selenium.webdriver.support.select import Select


class TestRegister:

    @pytest.fixture
    def user_data(self, browser):
        self.first_name = "Casey"
        self.last_name = "Li"
        self.phone = "+44 1234 567890"
        self.email = "casey.l@gmail.com"

        self.address = "44 Warren St"
        self.city = "Widnes"
        self.state = "Cheshire"
        self.postal_code = "WA8 0TA"
        self.country = "UNITED KINGDOM"

        self.user_name = "Cas_Li"
        self.password = "Qwert12"

    def test_register(self, browser, user_data):
        browser.get("http://demo.guru99.com/test/newtours/register.php")

        first_name = browser.find_element_by_name("firstName")
        first_name.send_keys(self.first_name)

        last_name = browser.find_element_by_name("lastName")
        last_name.send_keys(self.last_name)

        phone = browser.find_element_by_name("phone")
        phone.send_keys(self.phone)

        email = browser.find_element_by_id("userName")
        email.send_keys(self.email)

        address = browser.find_element_by_name("address1")
        address.send_keys(self.address)

        city = browser.find_element_by_name("city")
        city.send_keys(self.city)

        state = browser.find_element_by_name("state")
        state.send_keys(self.state)

        postal_code = browser.find_element_by_name("postalCode")
        postal_code.send_keys(self.postal_code)

        select = Select(browser.find_element_by_name("country"))
        select.select_by_value(self.country)

        user_name = browser.find_element_by_id("email")
        user_name.send_keys(self.user_name)

        password = browser.find_element_by_name("password")
        password.send_keys(self.password)
        password_confirm = browser.find_element_by_name("confirmPassword")
        password_confirm.send_keys(self.password)

        submit_button = browser.find_element_by_name("submit")
        submit_button.click()

        msg1 = browser.find_element_by_xpath("//table//tr[3]//p[1]//b")
        msg2 = browser.find_element_by_xpath("//table//tr[3]//p[3]//b")

        assert self.first_name and self.last_name in msg1.text
        assert self.user_name in msg2.text

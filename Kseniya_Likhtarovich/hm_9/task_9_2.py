"""
Создайте класс инвестиция: сумма инвестиции и срок.

Написать класс Bank: метод deposit возвращает сумму дохода.
"""


class Investment:
    """Класс для представления инвестиций."""
    def __init__(self, amount, term):
        """Устанавливает атрибуты для investment."""
        self.amount = amount
        self.term = term


class Bank(Investment):
    """Банковский вклад."""
    def deposit(self):
        """Возвращает сумму на счету пользователя."""
        period = self.term * 12
        for i in range(period):
            self.amount += self.amount * 0.10 / 12
        print(f"Сумма: {round(self.amount, 2)} BYN")


res_sum = Bank(1500, 1)
res_sum.deposit()

"""
Напишите декоратор.

Проверяет тип параметров функции и конвертирует их.
"""


def typed(datatype):
    """Конвертирует аргументы в переданный тип данных."""
    def decorator(func):
        def wrapper(*args):
            if datatype == "str":
                args = map(str, args)
            elif datatype == "int":
                args = map(int, args)
            elif datatype == "float":
                args = map(float, args)
            return func(*args)
        return wrapper

    return decorator


@typed(datatype="str")
def add(a, b):
    return a + b


print(add("3", 5))
print(add(5, 5))
print(add('a', 'b'))


@typed(datatype="int")
def add(a, b, c):
    return a + b + c


print(add(5, "6", 7))


@typed(datatype="float")
def add(a, b, c):
    return a + b + c


print(add(0.1, "0.2", 0.4))

from selenium.webdriver.common.by import By


class LoginPageLocators:

    LOCATOR_ACCOUNT = (By.CLASS_NAME, "navigation_page")
    LOCATOR_ERROR = (By.XPATH, "//div[@class='alert alert-danger']/ol/li")
    LOCATOR_EMAIL = (By.ID, "email")
    LOCATOR_PASSWD = (By.ID, "passwd")
    LOCATOR_SUBMIT = (By.ID, "SubmitLogin")

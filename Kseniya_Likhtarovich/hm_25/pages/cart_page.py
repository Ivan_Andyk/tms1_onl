from Kseniya_Likhtarovich.hm_25.pages.base_page import BasePage
from Kseniya_Likhtarovich.hm_25.locators.cart_page_locators\
    import CartPageLocators


class CartPage(BasePage):

    def should_be_cart_page(self):
        cart_text = self.find_element(CartPageLocators.LOCATOR_CART)
        assert cart_text.text == "SHOPPING-CART SUMMARY"

    def cart_is_empty(self):
        cart_empty = self.find_element(CartPageLocators.LOCATOR_EMPTY_CART)
        assert cart_empty.text == "Your shopping cart is empty."

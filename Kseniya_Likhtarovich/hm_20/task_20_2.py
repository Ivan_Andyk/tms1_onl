import wikipediaapi


def test_wiki_api():
    title = "Python (programming language)"

    wiki_lang = wikipediaapi.Wikipedia("en")
    page = wiki_lang.page(title)

    assert title == page.title, f"{page.title} != {title}"

from operator import attrgetter


class Flower:
    def __init__(self, name, freshness, color, length, price):
        self.name = name
        self.freshness = freshness
        self.color = color
        self.length = length
        self.price = price

    # this is a string view of Flower instances
    def __repr__(self):
        return repr((self.name))


class Bouquet:
    def __init__(self):
        self.flowers = []

    # adding flowers to bouquet
    def add_flower(self, *flowers):
        for i in flowers:
            self.flowers.append(i)

    # calculating total price for bouquet
    def get_bouquet_price(self):
        total_price = 0
        for j in self.flowers:
            total_price += j.price
        print(f"Bouquet price: {total_price}")

    # calculating fading time for bouquet
    def get_fading_time(self):
        fading_time = 0
        for k in self.flowers:
            fading_time += k.freshness
        bouquet_fading_time = fading_time / len(self.flowers)
        print(f"Your bouquet will fade after {bouquet_fading_time} days")

    # Soring flowers by parameter
    def sort_flowers(self, parameter):
        sorted_flowers = sorted(self.flowers, key=attrgetter(parameter))
        print(f"Your bouquet was sorted by {parameter}: {sorted_flowers}")

    # Flower search by parameter
    def find_flowers_by_param(self, parameter, value):
        result = [s.name for s in self.flowers
                  if getattr(s, parameter) == value]
        if result == []:
            print(f"Sorry. Found nothing by {parameter}: {value}")
        else:
            print(f"Found {''.join(result)} by {parameter}: {value}")

    # Flower search by name
    def find_flower_by_name(self, name):
        all_names = [m.name for m in self.flowers]
        if name in all_names:
            print(f"Your flower {name} is in this bucket")
        else:
            print("Sorry. Your flower is in another bouquet")


# Test data
rose = Flower("Rose", 10, "red", 30, 100)
tulip = Flower("Tulip", 15, "orange", 20, 50)
camomile = Flower("Camomile", 20, "white", 25, 10)

bouquet = Bouquet()
bouquet.add_flower(rose, tulip, camomile)

# Test cases
bouquet.get_bouquet_price()
bouquet.get_fading_time()
bouquet.sort_flowers(parameter="price")
bouquet.sort_flowers(parameter="color")
bouquet.find_flowers_by_param("name", "Test")
bouquet.find_flowers_by_param("price", 50)
bouquet.find_flower_by_name("Camomile")
bouquet.find_flower_by_name("Test")

a = {"a": 1, "b": 2, "c": 3}
b = {"c": 3, "d": 4, "e": 5}

# Get the keys for future dict
d_join = {**a, **b}
result_keys = list(d_join)

# Get two lists with values from a and b
x = list(a.values())
y = list(b.values())

# Preparing dicts
dict_1 = {}
for f in x:
    if f not in y:
        dict_1[f] = "None"
    else:
        dict_1[f] = f

dict_2 = {}
for e in y:
    if e not in x:
        dict_2[e] = "None"
    else:
        dict_2[e] = e

# Converting dicts to lists
list_1 = list(map(list, dict_1.items()))
list_2 = list(map(list, dict_2.items()))

# Reversing second list
list_3 = [list(reversed(i)) for i in list_2]

# Adding up list_1 and list_3
list_1.extend(list_3)

# Removing duplicate from list
result_list = []
for h in list_1:
    if h not in result_list:
        result_list.append(h)

# Making dict with result_keys and result_list
result = dict(zip(result_keys, result_list))
print(f"Thanks God we got this: {result}")

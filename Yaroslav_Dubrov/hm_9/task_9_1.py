class Book:
    def __init__(self, name, author, pages_number,
                 isbn, is_reserved, is_out, username):
        self.name = name
        self.author = author
        self.pages_number = pages_number
        self.isbn = isbn
        self.is_reserved = is_reserved
        self.is_out = is_out
        self.username = username


class User:
    def __init__(self, username):
        self.username = username

    def get_book(self, book):
        if book.is_out:
            print(f"This book is out. {self.username} can\'t take it")
        else:
            book.is_out = True
            book.username = self.username
            print(f"{self.username} successfully take this book")

    def return_book(self, book):
        if book.is_out is True and book.username == self.username:
            book.is_out = False
            book.username = ""
            print(f"{self.username} successfully return this book")
        else:
            print(f"{self.username} can\'t return this book. "
                  f"{book.username} are using it now")

    def reserve_book(self, book):
        if book.is_reserved is True or book.is_out is True:
            print(f"{self.username} can\'t reserve this book. "
                  f"It\'s out or reserved by {book.username}")
        elif book.is_reserved is False and book.is_out is False:
            book.is_reserved = True
            book.username = self.username
            print(f"{self.username} have reserved this book")

    def unreserve_book(self, book):
        if book.is_reserved is True:
            book.is_reserved = False
            book.username = ""
            print(f"{self.username} have unreserved this book")
        else:
            print(f"{self.username} can\'t unreserve this book")


# Test data below
book_1 = Book(
    "Jungle Book", "R. Kipling", 200, "9780194227469", False, False, "")
book_2 = Book(
    "Tom Sawyer", "M. Twain", 500, "9780606160162", False, False, "")

user_1 = User("John")
user_2 = User("Bob")
user_3 = User("Mary")


# Some test cases
user_3.get_book(book_1)
user_1.get_book(book_1)
user_1.return_book(book_1)
user_3.return_book(book_1)
user_1.get_book(book_1)

user_3.reserve_book(book_2)
user_1.reserve_book(book_2)
user_3.unreserve_book(book_2)
user_1.reserve_book(book_2)

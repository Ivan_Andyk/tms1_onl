numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]


def func(nmb):
    for i in nmb:
        if i > 0:
            yield i


for number in func(numbers):
    print(number, end="  ")

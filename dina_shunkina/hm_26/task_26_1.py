"""Напечатайте номер заказа, дату заказа и количество для каждого заказа,
Который продал продавец под номером: 5002"""

import mysql.connector as mysql

db = mysql.connect(
    host="localhost",
    user="root",
    passwd="",
    database="task_26"
)
cursor = db.cursor()
query = "SELECT ord_no, ord_date, " \
        "purch_amt FROM orders WHERE salesman_id = 5002"
cursor.execute(query)
print(cursor.fetchall())

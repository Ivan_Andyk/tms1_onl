from selenium import webdriver

browser = webdriver.Chrome("./chromedriver")
browser.implicitly_wait(5)
browser.get("http://thedemosite.co.uk/login.php")

try:
    email_input = browser.find_element_by_css_selector("[name='username']")
    email_input.send_keys("test@test.com")
    password_input = browser.find_element_by_css_selector("[name='password']")
    password_input.send_keys("1234567")
    button = browser.find_element_by_css_selector("[name='FormsButton2']")
    button.click()
    txt = browser.find_element_by_xpath("//b[text()='**Failed Login**']")
    print(txt.text)
finally:
    browser.quit()

"""работа с json файлом
Разработайте поиск учащихся в одном классе, посещающих одну секцию.
Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)
"""

import json

with open('students.json', 'r') as f:
    students_file = json.load(f)


def search_by_class_and_section(param, value):
    students_list = []
    for i in students_file:
        if i[param] == value:
            students_list.append(i["Name"])
    return students_list


def filter_by_gender(param, value):
    students_list = []
    for i in students_file:
        if i[param] == value:
            students_list.append(i["Name"])
    if value == "W":
        return f"Women: {students_list}"
    else:
        return f"Men: {students_list}"


def search_by_name(name):
    for i in students_file:
        if name in i["Name"]:
            return i


print(search_by_class_and_section("Class", "3a"))
print(search_by_class_and_section("Club", "Chess"))
print(filter_by_gender("Gender", "M"))
print(search_by_name("Yuna "))

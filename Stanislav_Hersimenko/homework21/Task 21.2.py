import xml.etree.ElementTree as ET
from xml.dom import minidom

tree = ET.parse('library.xml')
root = tree.getroot()


def library():
    for book in root.findall('book'):
        author = book.find('author').text
        price = book.find('price').text
        title = book.find('title').text
        description = book.find('description').text
        print(f"{author}; {price}; {title}; {description}")


library()

print('\n')

root2 = minidom.parse('library.xml')
books = root2.getElementsByTagName('book')
books_dict = []

for book in books:
    id = book.getAttribute('id')
    author = book.getElementsByTagName('author')[0].firstChild.nodeValue
    title = book.getElementsByTagName('title')[0].firstChild.nodeValue
    price = book.getElementsByTagName('price')[0].firstChild.nodeValue
    description = book.getElementsByTagName('description'
                                            '')[0].firstChild.nodeValue
    books_dict.append({'id': id,
                       'author': author,
                       'title': title,
                       'price': price,
                       'description': description})


def find_author(books, author):
    res = [i for i in books if str(author.lower()) in i['author'].lower()]
    print(f'Finding by Author: {author}')
    for root in res:
        print(root)


def find_price(books, price):
    print(f'Finding by price: {price}')
    res = [i for i in books if float(i['price']) == float(price)]
    for root in res:
        print(root)


def find_title(books, title):
    res = [i for i in books if str(title.lower()) in i['title'].lower()]
    print(f'Finding by title: {title}')
    for root in res:
        print(root)


def find_description(books, description):
    res = [i for i in books if str(description.lower())
           in i['description'].lower()]
    print(f'Finding by description: {description}')
    for root in res:
        print(root)


find_author(books_dict, 'ardella')
find_price(books_dict, 5.95)
find_title(books_dict, 'Clean Code')
find_description(books_dict, 'A Very Simple Introduction')

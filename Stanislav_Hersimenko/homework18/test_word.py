import unittest
from unittest import expectedFailure
from main_word import word


class Test_words(unittest.TestCase):

    def test_word_duplicate_together(self):
        actual = word("aabcd" + " ")
        expected = word("a2bcd")
        self.assertTrue(actual, expected)

    def test_word_duplicate_other_position(self):
        actual = word("aabca" + " ")
        expected = word("a2bca")
        self.assertTrue(actual, expected)

    def test_word_without_duplicate(self):
        actual = word("abcde" + " ")
        expected = word("abcde")
        self.assertTrue(actual, expected)

    def test_word_multiple_duplicate(self):
        actual = word("abeehhhhhccced" + " ")
        expected = word("abe2h5c3ed")
        self.assertTrue(actual, expected)

    def test_word_one_multiple_symbol(self):
        actual = word("aaaaa" + " ")
        expected = word("a5")
        self.assertTrue(actual, expected)

    def test_word_figures(self):
        actual = word("111" + " ")
        expected = word("13")
        self.assertTrue(actual, expected)

    @expectedFailure
    def test_word_negative(self):
        actual = word("aabca" + " ")
        expected = word("a3bc")
        self.assertFalse(actual, expected)

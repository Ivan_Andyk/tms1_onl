a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
ab = {**a, **b}
for Value in ab:
    if Value in a and Value in b:
        ab[Value] = [a[Value], b[Value]]
    elif Value in ab:
        ab[Value] = [ab[Value], None]
print(ab)

from selenium import webdriver
import time


def test_login():
    url = "http://thedemosite.co.uk/login.php"
    path = "C:/chromedriver.exe"
    Username = "fdgghgfhgf"
    Password = "dfgdfg"
    driver = webdriver.Chrome(path)
    driver.get(url)
    Username_input = driver.find_element_by_xpath(
        '/html/body/table/tbody/tr/td[1]/form/'
        'div/center/table/tbody/tr/td[1]/table/'
        'tbody/tr[1]/td[2]/p/input')
    Username_input.send_keys(Username)
    Password_input = driver.find_element_by_xpath(
        '/html/body/table/tbody/tr/td[1]/form/'
        'div/center/table/tbody/tr/td[1]/table/'
        'tbody/tr[2]/td[2]/p/input')
    Password_input.send_keys(Password)
    Test_Login = driver.find_element_by_xpath(
        '/html/body/table/tbody/tr/td[1]/form/'
        'div/center/table/tbody/tr/td[1]/table/'
        'tbody/tr[3]/td[2]/p/input')
    Test_Login.click()
    Login = driver.find_element_by_xpath(
        '/html/body/table/tbody/tr/td[1]/big/'
        'blockquote/blockquote/font/center/b').text
    time.sleep(1)
    assert Login == "**Successful Login**"
    driver.quit()

from pages.main_page import MainPage
from pages.cart_page import CartPage


def test_cart_empty(browser):
    main_page = MainPage(browser)
    main_page.open_main_page()
    main_page.should_be_t_shirts()
    main_page.should_be_add_to_card()
    main_page.should_be_checkout()
    main_page.open_cart_page()

    cart_page = CartPage(browser)
    cart_page.should_be_cart_not_empty()
    cart_page.should_be_deleted()
    cart_page.should_cart_update()
    cart_page.should_be_cart_page_title()
    cart_page.should_be_cart_empty()

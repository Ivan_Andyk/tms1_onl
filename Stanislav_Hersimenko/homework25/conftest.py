from selenium import webdriver
import pytest


@pytest.fixture
def browser():
    driver = webdriver.Chrome("C:/chromedriver.exe")
    driver.implicitly_wait(5)
    yield driver
    driver.quit()

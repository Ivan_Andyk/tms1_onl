def Validate(CreditCard):
    def elements(number):
        return [int(value) for value in str(number)]

    element = elements(CreditCard)
    uneven = element[-1::-2]
    even = element[-2::-2]
    result = 0
    result = result + sum(uneven)
    for value2 in even:
        result = result + sum(elements(value2 * 2))
    return result % 10


def Validate_result(CreditCard_number):
    return Validate(CreditCard_number) == 0


can_input_card_number = True
while can_input_card_number:
    try:
        total_result = Validate_result(int(input("Enter the card number: ")))
        total_result_string = f'{total_result}'
        total_result_string_list = list(total_result_string)
        print('Validation status:' + str(total_result))
        can_input_card_number = False
    except ValueError:
        print("Only numbers allowed.")
        can_input_card_number = False

import pytest


@pytest.mark.task_21_1
def test_demosite_login(browser):
    login = 'igor_vanin'
    password = '1234qwer'
    input_name = 'input[name="username"]'
    input_pass = 'input[name="password"]'
    button = '[type="button"]'
    text_succeful = '//b[contains(text(), "**Successful Login**")]'
    browser.get('http://thedemosite.co.uk/login.php')
    browser.find_element_by_css_selector(input_name).send_keys(login)
    browser.find_element_by_css_selector(input_pass).send_keys(password)
    browser.find_element_by_css_selector(button).click()
    title = browser.find_element_by_xpath(text_succeful).text
    assert title == "**Successful Login**"

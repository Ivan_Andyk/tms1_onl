from collections import Counter

text = "Hello World!"
lwr_text = text.lower()
b = list(lwr_text)
# Шаблоны проверки спец.символов и т.д
not_valid_character = ['!', ',', '&', '@', '#', '?', ' ', '-']

for i in not_valid_character:
    if i in b:  # Мапим значения из шаблона
        x = str(i)      # Преобразовываем запрещенные символы
        b.remove(x)     # Удаляем запрещенные символы

most_recurring_char = Counter(b)
print(most_recurring_char.most_common(1))

def sort_numbers(func):
    def wrapper(*args):
        number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                        5: 'five', 6: 'six', 7: 'seven', 8: 'eight',
                        9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve',
                        13: 'thirteen', 14: 'fourteen', 15: 'fifteen',
                        16: 'sixteen', 17: 'seventeen', 18: 'eighteen',
                        19: 'nineteen'}

        list_values = []
        list_keys = []

        for i in args:
            for key, val in number_names.items():
                if i == key:
                    list_values.append(val)
        list_values.sort()

        for i in list_values:
            for key, val in number_names.items():
                if i == val:
                    list_keys.append(key)
        func(list_keys)

    return wrapper


@sort_numbers
def lex_numbers(numbers):
    return print(*numbers)


"""Test"""
lex_numbers(1, 2, 4, 3)

from pages.base_page import BasePage
from locators.Account_Page_Locators import AccountPageLocators


class AccountPage(BasePage):

    def should_be_account_page(self):
        my_account_text = self.find_element(
            AccountPageLocators.LOCATOR_MY_ACCOUNT_TEXT).text
        assert my_account_text == "MY ACCOUNT"

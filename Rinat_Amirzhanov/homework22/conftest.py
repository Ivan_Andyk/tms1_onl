import pytest

from fixture.application import Application


@pytest.fixture()
def apps(request):
    fixture = Application()
    request.addfinalizer(fixture.destroy)
    return fixture

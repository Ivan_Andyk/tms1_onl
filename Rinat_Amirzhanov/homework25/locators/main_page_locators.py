from selenium.webdriver.common.by import By


class MainPageLocators:

    LOCATOR_SIGN_IN_BUTTON = (By.CLASS_NAME, "login")
    LOCATOR_BEST_SELLERS_BUTTON = (By.CLASS_NAME, "blockbestsellers")
    LOCATOR_CART_PAGE = (By.XPATH, "//a[@title='View my shopping cart']")
    LOCATOR_WOMEN_TAB = (By.XPATH, "//a[@title='Women']")
    LOCATOR_DRESSES_TAB = (By.XPATH, "//*[@id='block_top_menu']/ul/li[2]/a")
    LOCATOR_TSHIRTS_TAB = (By.XPATH, "//*[@id='block_top_menu']/ul/li[3]/a")

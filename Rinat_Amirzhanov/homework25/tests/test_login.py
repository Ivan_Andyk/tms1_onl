from Rinat_Amirzhanov.homework25.pages.login_page import LoginPageHelper
from Rinat_Amirzhanov.homework25.pages.main_page import MainPageHelper


def test_login_error(browser):
    main_page = MainPageHelper(browser)
    main_page.open_main_page()
    main_page.open_login_page()
    login_page = LoginPageHelper(browser)
    login_page.should_be_login_page()
    login_page.login("adst@gmail.com", "12345")
    login_page.login_error()

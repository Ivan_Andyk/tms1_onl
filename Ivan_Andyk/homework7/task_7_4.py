list_eng = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
            "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
list_ru = ["а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л",
           "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш",
           "щ", "ъ", "ы", "ь", "э", "ю", "я"]

step1 = int(input("Выберите язык: 1 - английский, 2 -русский: "))
while step1 == 1:
    step2 = int(input("Select: 1 - encode, 2 -decode: "))
    while step2 == 1:
        step = int(input("Select the step: "))
        while step > 26:
            step -= 26
        phrase = (input("Enter your message: ")).lower()
        phrase_code = []
        phrase = phrase.split()
        for word in phrase:
            list_word = []
            for letter in word:
                i = list_eng.index(letter)
                if i + step > 25:
                    i -= 26
                list_word.append(list_eng[i - step])
            phrase_code.append("".join(list_word))
        print(" ".join(phrase_code))
    while step2 == 2:
        step = int(input("Select the step: "))
        while step > 26:
            step -= 26
        phrase = (input("Enter your message: ")).lower()
        phrase_code = []
        phrase = phrase.split()
        for word in phrase:
            list_word = []
            for letter in word:
                i = list_eng.index(letter)
                if i == [",", ".", ":", ";", "!", "?"]:
                    continue
                elif i + step > 25:
                    i -= 26
                list_word.append(list_eng[i + step])
            phrase_code.append("".join(list_word))
        print(" ".join(phrase_code))
while step1 == 2:
    step2 = int(input("Выбери: 1 - зашифровать, 2 - расшифровать: "))
    while step2 == 1:
        step = int(input("Выбери шаг: "))
        while step > 33:
            step -= 33
        phrase = (input("Введите вашу фразу: ")).lower()
        phrase_code = []
        phrase = phrase.split()
        for word in phrase:
            list_word = []
            for letter in word:
                i = list_ru.index(letter)
                if i == [",", ".", ":", ";", "!", "?"]:
                    continue
                elif i + step > 32:
                    i -= 33
                list_word.append(list_ru[i - step])
            phrase_code.append("".join(list_word))
        print(" ".join(phrase_code))
    while step2 == 2:
        step = int(input("Выбери шаг: "))
        while step > 33:
            step -= 33
        phrase = (input("Введите вашу фразу: ")).lower()
        phrase_code = []
        phrase = phrase.split()
        for word in phrase:
            list_word = []
            for letter in word:
                i = list_ru.index(letter)
                if i == [",", ".", ":", ";", "!", "?"]:
                    continue
                elif i + step > 32:
                    i -= 33
                list_word.append(list_ru[i + step])
            phrase_code.append("".join(list_word))
        print(" ".join(phrase_code))

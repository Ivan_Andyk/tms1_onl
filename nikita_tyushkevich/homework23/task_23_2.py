# Fill form & Validation check

from selenium import webdriver

browser = webdriver.Chrome()
browser.implicitly_wait(5)
browser.get("https://ultimateqa.com/filling-out-forms/")


def test_form():
    name_field = browser.find_element_by_id('et_pb_contact_name_0')
    name_field.send_keys('Nikita Tyushkevich')
    message = browser.find_element_by_id('et_pb_contact_message_0')
    message.send_keys('Works well!')
    submit = browser.find_element_by_xpath('//div[@id="et_pb_contact_form_0'
                                           '"]//button')
    submit.click()
    message_text = browser.find_element_by_xpath("//div[@class='et-pb-contact"
                                                 "-message']/p")
    assert message_text.text == 'Thanks for contacting us'


def test_name_field():
    browser.refresh()
    name_field = browser.find_element_by_id('et_pb_contact_name_0')
    name_field.send_keys('Nikita Tyushkevich')
    submit = browser.find_element_by_xpath('//div[@id="et_pb_contact_form_0'
                                           '"]//button')
    submit.click()
    message_text = browser.find_element_by_xpath("//div[@class='et-pb-contact"
                                                 "-message']/p")
    message_text2 = browser.find_element_by_xpath("//div[@class='et-pb-"
                                                  "contact-message']//li")
    assert message_text.text == 'Please, fill in the following fields:'
    assert message_text2.text == 'Message'


def test_message_field():
    browser.refresh()
    message = browser.find_element_by_id('et_pb_contact_message_0')
    message.send_keys('Check validation')
    submit = browser.find_element_by_xpath('//div[@id="et_pb_contact_form_0'
                                           '"]//button')
    submit.click()
    message_text = browser.find_element_by_xpath("//div[@class='et-pb-contact"
                                                 "-message']/p")
    message_text2 = browser.find_element_by_xpath("//div[@class='et-pb-"
                                                  "contact-message']//li")
    assert message_text.text == 'Please, fill in the following fields:'
    assert message_text2.text == 'Name'
    browser.quit()

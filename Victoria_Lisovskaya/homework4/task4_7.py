import re
from collections import Counter
from collections import OrderedDict
input1 = "How do you do?"
# remove all chars which aren't letters
new = ''.join(re.findall('[a-z]', input1.lower()))
# use Counter dictionary for getting of statistic
c = Counter(new)
c.most_common(len(new))
# get sorted dictionary
a = OrderedDict(c.most_common(len(new)))
# get the first key
for k in a.keys():
    if k:
        print(k)
        break
